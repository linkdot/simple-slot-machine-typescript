# Simple Slot Machine - Typescript

This a simple slot machine game coded in typescript.


## Getting Started

This repository is a stripped and compiled version of the project. If you need to recompile typescripts, there are node modules you need include.


## Prerequisites

* install Nodejs with NPM

These are the node modules you need to install to start compiling your project.

* [typescript](https://www.npmjs.com/package/typescript) - typescript module
* [ts-loader](https://www.npmjs.com/package/ts-loader) - load typescript for webpack
* [pixi](https://github.com/pixijs/pixi-typescript) - pixi written in typescript
* [webpack](https://www.npmjs.com/package/webpack) - Compiling typescript
* [webpack-cli](https://www.npmjs.com/package/webpack-cli) - CLI for webpack debugging


### Pixi Typescript

Install pixi typescript on the project by typing
```
npm install @types/pixi.js --save-dev
```
on your root project using cmd.


### Webpack and Webpack CLI

Next type these commands
```
npm install webpack
npm install webpack-cli
```
or
```
npm install webpack webpack-cli
```


### Ready for Development

After installing all the modules, it can now be compile using
```
webpack --watch
```
as a command.


### Run

Install http-server by 
```
npm install http-server
```
and run you project bY this command
```
http-server -p[PORT]
```


