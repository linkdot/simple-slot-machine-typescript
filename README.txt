**********************README.txt******************************

Game: Slot Machine Demo 
Developed by: Johnry Bautista

**************************************************************

Description:

	Slot game demo for job exam. Used Typescript and PIXI.js 
	for rendering

Tools/Modules Used:
	* Webpack - Used for converting ts to js
	* PIXI - Rendering UI
	* Typescript - Scripting

Rebuild/Compile????

	Modules
		npm install @types/pixi.js --save-dev
		npm install webpack
		npm install typescript
		npm install http-server

	Converting ts to js
		webpack --watch  <-------------------- /*On project Directory*/
		


