 import {Reel} from './reel'
 import {SlotState} from './constants'
 import {Constants} from './constants'

//This is where i controll all the reels
export class ReelController {

	private static _instance: ReelController
	reels:Reel[] = new Array(Constants.ReelsNo)
	state:SlotState = SlotState.READY
	currentIndex:number = 1
	interval:number
	reelAudio:any
	callback:any

    private constructor()
    {
        //...
        //Just add a reel audio for spinning
        this.reelAudio = new Audio("./audios/Reel_Spin.mp3")
        this.reelAudio.loop = true


    }

    public static getInstance():ReelController
    {
        //Singleton instance
        return ReelController._instance || (ReelController._instance = new ReelController())
    }

    addCallback(_callback:any):void {

    	ReelController.getInstance().callback = _callback
	}

    setDelay():void {

    	setTimeout(ReelController.getInstance().increment, Constants.Delay)
    }

    increment():void {

    	if(ReelController.getInstance().currentIndex < Constants.ReelsNo) {

    		ReelController.getInstance().currentIndex += 1
    		setTimeout(ReelController.getInstance().increment, Constants.Delay)
    	}
    }

    //Calls everyframe
    //The main reel controller
    run():void {

    	switch (this.state) {
    		case SlotState.READY:
    			//Ready state
    			break;

    		case SlotState.ROLLING:

    			//This is where i run all the reels
		    	for(var x = 0; x < ReelController.getInstance().currentIndex; x++) {

		    		this.reels[x].runReel()
		    	} 

                if(this.reels[Constants.ReelsNo -1].isDone)
                    this.state = SlotState.END

    			break;

    		case SlotState.END:

    			for(var y = 0; y < Constants.ReelsNo; y++) {

		    		this.reels[y].resetReel()
		    	}

		    	//Stops the reel audio
		    	this.reelAudio.pause()
		    	this.reelAudio.currentTime = 0
		    	ReelController.getInstance().currentIndex = 1
		    	this.state = SlotState.READY

		    	//Resets the 
		    	if(ReelController.getInstance().callback)
		    		ReelController.getInstance().callback()

    			break;
    	}
    }
}