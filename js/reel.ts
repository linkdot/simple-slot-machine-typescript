import * as Utils from './utils'
import {Constants} from './constants'

//Reel class
//Just like any display objects
//Inherited from PIXI.Sprite
export class Reel extends PIXI.Sprite {

	reelSeries:number[]
	moveLimit:number
	isDone:boolean = false

	//Constructor
	constructor(_texture:PIXI.Texture = PIXI.loader.resources["transparent"].texture) {

		super(_texture)
		this.init(0, 0, [1,2,3,4,5,6,7,8,9,10])
	}

	//Initialize the height and the reel Series
 	init(_cellHeight:number, _reelWidth:number, _reelSeries:number[]):void {

 		this.height = _cellHeight * _reelSeries.length
 		this.width = _reelWidth
 		this.reelSeries = _reelSeries
	}

	//Seeding the reel with Icons depending on the reelseries
	seed():void {

		this.removeChildren()

		 for(var x = 0; x < this.reelSeries.length; x++) {

 			let id = this.reelSeries[x].toString();
 			var cell = new PIXI.Sprite(PIXI.loader.resources[id].texture)

 			let cellHeight = Utils.getRealHeight(this.texture)/this.reelSeries.length

 			cell.anchor.set(0, 1)
			cell.width = Utils.getRealWidth(this.texture)
			cell.height = cellHeight
			cell.position.x = Utils.getOriginX(this)
			cell.position.y = Utils.getOriginY(this) + Utils.getRealHeight(this.texture) - (cellHeight * x)
 			this.addChild(cell)
 		}
	}

	//THIS IS WHERE THE MOVING OF REEL
	runReel():void {

		if(this.position.y <= this.moveLimit) {

			this.position.y += Constants.Speed

		} else {

			this.isDone = true
			var audio = new Audio("./audios/Landing_1.mp3")
			audio.play()
		}
	}

	//Ressting the position of the reel
	resetReel():void {

		//Retaining the last for series
		let newStartArr = this.reelSeries.slice(this.reelSeries.length - 4, this.reelSeries.length)

		let addArr:number[] = new Array(26);

		//Randomize next series
		for(let z = 0; z < addArr.length; z++) {

			addArr[z] =Math.floor((Math.random() * 13) + 1);
		}

		this.reelSeries = newStartArr.concat(addArr)

		//Reseed
		this.seed()

		//Reset position
		this.position.y = (this.height / this.reelSeries.length) * 4

		//Set done to false
		this.isDone = false

	}
}