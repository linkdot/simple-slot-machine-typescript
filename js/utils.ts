//Methods
//Useful Utilities
//Specially when you are layouting objects

//Maintains the ratio of dimension of the sprite {Width and Height}
export function getPreserveWidthFromHeight(_height:number, _aspectRatio:number[]) : number{

	return (_height / _aspectRatio[1]) * _aspectRatio[0]
}

export function getPreserveHeightFromWidth(_width:number, _aspectRatio:number[]) : number {

	return (_width / _aspectRatio[0]) * _aspectRatio[1]
}

//Get the half of a Number
export function getHalf(_value:number) : number {

	return _value/2
}

//Get the value of a percentage given a max value
export function getPercentV(_value:number, _percent:number):number{

	return _value * _percent
}

//When adding a sprite inside a Sprite
//You just cant access the real width and height of a sprite Instead, use these {Width, Height}
export function getRealWidth(_texture:PIXI.Texture):number {

	return _texture.width / _texture.baseTexture.resolution;
}

export function getRealHeight(_texture:PIXI.Texture):number {

	return _texture.height / _texture.baseTexture.resolution;
}

//Also this one
//This is to get the (0, 0) of a parent Sprite {Width, Height}
export function getOriginX(_sprite:PIXI.Sprite):number {

	return (getRealWidth(_sprite.texture) * _sprite.anchor.x) / -1
}

export function getOriginY(_sprite:PIXI.Sprite):number {

	return (getRealHeight(_sprite.texture) * _sprite.anchor.y) / -1
}