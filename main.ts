 import * as Utils from './js/utils'
 import {Constants} from './js/constants'
 import {SlotState} from './js/constants'
 import {Reel} from './js/reel'
 import {ReelController} from './js/reelcontroller'
 

class SlotMachine {

	width:number
	height:number

	//Constructor of slot Machine
	constructor(){

		this.width = window.innerWidth
		this.height = window.innerHeight
		this.load();
	}

	load () {
		//Here we load of our sprites
		PIXI.loader.add("slotframe", "images/slotOverlay.png")
		.add("reelsbg", "images/frameBackground.jpg")
		.add("1", "images/01.png")
		.add("2", "images/02.png")
		.add("3", "images/03.png")
		.add("4", "images/04.png")
		.add("5", "images/05.png")
		.add("6", "images/06.png")
		.add("7", "images/07.png")
		.add("8", "images/08.png")
		.add("9", "images/09.png")
		.add("10", "images/10.png")
		.add("11", "images/11.png")
		.add("12", "images/12.png")
		.add("13", "images/13.png")
		.add("transparent", "images/empty.png")
		.add("btnNormal", "images/btn_spin_normal.png")
		.add("btnHover", "images/btn_spin_hover.png")
		.add("btnPressed", "images/btn_spin_pressed.png")
		.add("btnDisabled", "images/btn_spin_disabled.png")
		.add("background", "images/background.jpg")
		.load(this.render)
	}

	render() {

		//INITIALIZE THE WHOLE CANVAS
		var renderer = PIXI.autoDetectRenderer(window.innerWidth, window.innerHeight, {backgroundColor : 0xffffff})
		document.body.appendChild(renderer.view)

		//Container of all of our Displayed objects
		var stage = new PIXI.Container()

		//background, frame and frame background
		var background = new PIXI.Sprite(PIXI.loader.resources["background"].texture)
		var frame = new PIXI.Sprite(PIXI.loader.resources["slotframe"].texture)
		var frameBg = new PIXI.Sprite(PIXI.loader.resources["reelsbg"].texture)

		//Textures for our Spin Button
		var textureBtnNormal = PIXI.loader.resources["btnNormal"].texture
		var textureBtnHover = PIXI.loader.resources["btnHover"].texture
		var textureBtnPressed = PIXI.loader.resources["btnPressed"].texture
		var textureBtnDisabled = PIXI.loader.resources["btnDisabled"].texture

		//Adjust psrite properties 
		//Layout all necessary displayed objects
		let frameHeight = Utils.getPercentV(window.innerHeight, 0.8)
		let FrameWidth = Utils.getPreserveWidthFromHeight(frameHeight, [frame.width, frame.height])

		background.width = window.innerWidth
		background.height = window.innerHeight

		frame.width = FrameWidth
		frame.height = frameHeight
		frame.position.x = Utils.getHalf(window.innerWidth)
		frame.anchor.set(0.5, 0.0)

		frameBg.anchor.set(0.5, 0.0)
		frameBg.width = Utils.getPercentV(frame.width, 0.935)
		frameBg.height = Utils.getPercentV(frame.height, 0.93)
		frameBg.position.x = Utils.getPercentV(frame.position.x, 0.99)
		frameBg.position.y = Utils.getPercentV(frame.height, 0.05)

		stage.addChild(background)
		stage.addChild(frameBg)
		stage.addChild(frame)

		stage.interactive = true

		//Used GRaphics for masking all 5 reels
		var mask = new PIXI.Graphics()
		mask.beginFill(0x555555)
		mask.position.x = Utils.getPercentV(frame.position.x, 0.99) - (frameBg.width/2)
		mask.position.y = Utils.getPercentV(frame.height, 0.05)
		mask.drawRect(0, 0, frameBg.width, frameBg.height)
		mask.endFill()
		stage.addChild(mask)

		//Initialized the button object for spinning
	 	var button = new PIXI.Sprite(textureBtnNormal)

	 	let btnHeight = Utils.getPercentV(window.innerHeight, 0.15)
		let btnWidth = Utils.getPreserveWidthFromHeight(btnHeight, [button.width, button.height])

		button.width = btnWidth
		button.height = btnHeight
		button.anchor.set(1,1)
		button.position.x = Utils.getPercentV(window.innerWidth, 0.98)
		button.position.y = Utils.getPercentV(window.innerHeight, 0.95)
		button.buttonMode = true
		button.interactive =true

		 button.on('pointerdown', onButtonDown)
        .on('pointerup', onButtonUp)
        .on('pointerupoutside', onButtonUp)
        .on('pointerover', onButtonOver)
        .on('pointerout', onButtonOut);

		stage.addChild(button)

		//Event Methods for button
		function onButtonDown() {

			if(ReelController.getInstance().state == SlotState.READY) {

				button.texture = textureBtnPressed
				// ReelController.getInstance().state = SlotState.ROLLING

			} else {

				button.texture = textureBtnDisabled
			}
		}

		function onButtonUp() {

			button.texture = textureBtnDisabled

			if(ReelController.getInstance().state == SlotState.READY) {

				ReelController.getInstance().state = SlotState.ROLLING
				ReelController.getInstance().reelAudio.play()
				ReelController.getInstance().setDelay()
			}
		}

		function onButtonOver() {
			button.texture = textureBtnHover
		}

		function onButtonOut() {

			if(ReelController.getInstance().state == SlotState.READY) {

				button.texture = textureBtnNormal

			} else {

				button.texture = textureBtnDisabled
			}
		}

		function onBtnCallback() {
			
			if(ReelController.getInstance().state == SlotState.READY) {

				button.texture = textureBtnNormal

			} else {

				button.texture = textureBtnDisabled
			}
		}

		ReelController.getInstance().addCallback(onBtnCallback)

		//Added reels for the slot machine
		for(var r = 0; r < Constants.ReelsNo; r++) {

			let widthInc = Utils.getRealWidth(frameBg.texture)/Constants.ReelsNo
			var reel = new Reel()
			reel.init(Utils.getRealHeight(frameBg.texture)/4, widthInc, Constants.InitialReel[r])
			reel.anchor.set(0, 1)
			reel.position.x = Utils.getOriginX(frameBg) + (widthInc * r)
			reel.position.y = Utils.getOriginY(frameBg) + Utils.getRealHeight(frameBg.texture)
			reel.moveLimit = Utils.getOriginY(frameBg) + (reel.height)
			frameBg.addChild(reel)

			reel.seed()

			//this is where i used the masking
			reel.mask = mask

			//Add all the reels on the Reelcontroller
			//Note: Reelcontroller is a singleton
			ReelController.getInstance().reels[r] = reel
		}

		//Call updating method
		animate()

		//Main update method
		function animate() {

		    requestAnimationFrame(animate)

		    ReelController.getInstance().run()
		    // render the container
		    renderer.render(stage)

		}
	}

}

// Construct our SLOT machine
// Starts here
let slotMachine = new SlotMachine()